from django.shortcuts import render,redirect
from django.urls import reverse
from django.http import HttpResponse
from account.models import UserInformation,FollowersModel
from .models import make_tiyoots_models,repository
from .forms import make_tiyoots_forms,repositoryMake
import time
import datetime




def setSession(request):
	request.session.set_expiry(4*60*60)
	print (request.session.get_expiry_age())

def make_tiyoots(request,username='no'):
	setSession(request)
	if request.method=='POST':
		print ('is_login' in request.session)
		if 'is_login' in request.session and username !='no' and 'username' in request.session:
			if request.session['is_login']=='loggedin' and request.session['username']==str(username):
				tiyoot_form=make_tiyoots_forms(request.POST)
				if tiyoot_form.is_valid():
					obj=make_tiyoots_models()
					user=UserInformation.objects.get(username=request.session['username'])
					obj.user=user
					obj.headline=tiyoot_form.cleaned_data['headline']
					obj.content=tiyoot_form.cleaned_data['content']
					obj.save()
					#request.session.set_expiry(60)
					return redirect(reverse('tiyoots',kwargs={'username':str(request.session['username'])}))
				else :
					tiyoot_form=make_tiyoots_forms()
					message='your data is not valid!!'
					return render(request,'posts/make_tiyoots.html',{'message':message,'form':tiyoot_form})
			else:
				return redirect("https://google.com")
		else:
			message='your connection is not safe!login page in 5 seconde'
			time.sleep(2)
			return redirect("login")
	else :
		if not 'is_login' in request.session:
			return redirect("login")
		message=''
		tiyoot_form=make_tiyoots_forms()
		return render(request,'posts/make_tiyoots.html',{'message':message,'form':tiyoot_form})




def repositoryView(request,username=None):
	setSession(request)
	if request.method=="POST" and "username" in request.session:
		print("hi")
		form=repositoryMake(request.POST,request.FILES)
		if form.is_valid():
			user=UserInformation.objects.get(username=str(request.session['username']))
			model=repository()
			model.user=user
			model.headline=form.cleaned_data['headline']
			model.codeFile=form.cleaned_data['codeFile']
			model.description=form.cleaned_data['description']
			model.codeTag=form.cleaned_data['codeTag']
			model.save()
			return redirect("https://google.com")
	else:
		form=repositoryMake()
	return render(request,"posts/repo.html",{'form':form})



def codesView(request,username=None):
	
	setSession(request)
	isExists=UserInformation.objects.filter(username=str(username)).exists()
	flag=None
	if isExists:
		data=repository.objects.filter(user__username=str(username))
		for i in data:
			i.headline=i.headline
			i.codeFile=i.codeFile.open(mode="r").read()
			i.description=i.description
			print(i.codeTag)
		flag=1
	else:
		flag=0
	print (request.session['username'])
	return render(request,"posts/codepage.html",{'codes':data,'flag':flag})




def tiyoots(request,username='no'):
	setSession(request)
	if 'is_login' in request.session and 'username' in request.session:
			try:
				print (request.session.get_expiry_age())
				#setSession(request)
				user=UserInformation.objects.get(username=str(username))
				followers=[f for f in FollowersModel.objects.filter(user=user)]
				followerss=[i for i in followers if i.follower==UserInformation.objects.get(username=str(request.session['username']))]
				data=[m for m in make_tiyoots_models.objects.filter(user=user).order_by('-datetime')]
				if followerss:
					return render(request,'posts/tiyoots.html',{'tiyoots':data,'is_follower':followerss,'followers':followers,'usern':str(username),'value_btn':'Unfollow'})
				else :
					return render(request,'posts/tiyoots.html',{'tiyoots':data,'is_follower':followerss,'usern':str(username),'value_btn':'Follow'})
			except Exception as e:
				return HttpResponse(str(e)+" yes")
	else :
		
		message='you should loggin first !!!'
		return redirect('login')


def follow(request,username='no'):
	setSession(request)
	if 'username' in request.session and request.method=="POST":
		followers=FollowersModel.objects.filter(user=UserInformation.objects.get(username=str(username)))
		is_following=[i for i in followers if i.follower==UserInformation.objects.get(username=str(request.session['username']))]
		if is_following:
			follower=UserInformation.objects.get(username=str(request.session['username']))
			user=UserInformation.objects.get(username=str(username))
			FollowersModel.objects.filter(follower=follower,user=user).delete()
		else :
			follower=UserInformation.objects.get(username=str(request.session['username']))
			user=UserInformation.objects.get(username=str(username))
			f=FollowersModel()
			f.user=user
			f.follower=follower
			f.save()
		return redirect(reverse('tiyoots',kwargs={'username':str(username)}))
	else:
		return redirect("https://google.com")



def logout(request,username='no'):
	if 'username' in request.session:
		request.session.flush()

		return redirect("login")
	else :
		return HttpResponse('<h1> log out !!!</h1>')



def whatsup(request,username='no'):
	setSession(request)
	print (request.session.get_expiry_age())
	time.sleep(2)
	print (request.session.get_expiry_age())
	following=FollowersModel.objects.filter(follower=UserInformation.objects.get(username=str(request.session['username'])))
	tiyoot_list=[make_tiyoots_models.objects.filter(user__username=i.user.username) for i in following if i.user != UserInformation.objects.get(username=str(request.session['username']))]
	tiyoot=[]
	is_user=True if (username==request.session['username']) else False
	for i in range(0,len(tiyoot_list)):
		for j in tiyoot_list[i]:
			tiyoot.append(j)
	tiyoot.sort(key=lambda x:x.datetime,reverse=True)
	return render(request,"posts/whatsup.html",{'tiyoots':tiyoot,'is_user':is_user,'usern':str(request.session['username'])})
