from rest_framework.serializers import ModelSerializer
from posts.models import make_tiyoots_models,repository


class tiyootsModelSerializer(ModelSerializer):
    class Meta:
        model=make_tiyoots_models
        fields='__all__'


class TiyootLoggedInSerializer(ModelSerializer):
    class Meta:
        model  = make_tiyoots_models
        fields = ('headline','content')

class TiyootDeleteSerializer(ModelSerializer):
    class Meta:
        model   =   make_tiyoots_models
        fields  =   '__all__'
        
class CodeListApiView(ModelSerializer):
    class Meta:
        model   =   make_tiyoots_models
        fields  =   '__all__'

class UpdateApiView(ModelSerializer):
    class Meta:
        model = make_tiyoots_models
        fields  = '__all__'