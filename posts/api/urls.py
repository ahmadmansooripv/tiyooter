from django.urls import path,include,re_path
from posts.api.views import postListSerializerAPIView,PostLoggedInRetriveApiView,PostDestroyApiView,codeApiListView,TiyootUpdateApiView



urlpatterns = [
    path('',postListSerializerAPIView.as_view(),name="api"),
    re_path(r'^(?P<id>[a-z0-9]+)/$',PostLoggedInRetriveApiView.as_view(),name="loggedin_api"),
    re_path(r'^(?P<id>[a-z0-9]+)/destroy/',PostDestroyApiView.as_view(),name="destroyer"),
    re_path(r'^code/(?P<username>[a-z0-9]+)/$',codeApiListView.as_view(),name="codelistview"),
    re_path(r'^update/(?P<id>[a-z0-9]+)/$',TiyootUpdateApiView.as_view(),name="updateapiview"),
]
