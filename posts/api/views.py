from rest_framework import generics
from posts.models import make_tiyoots_models,repository
from posts.api.serializer import tiyootsModelSerializer,TiyootLoggedInSerializer,TiyootDeleteSerializer,CodeListApiView,UpdateApiView



class postListSerializerAPIView(generics.ListAPIView):
   
    queryset            =   make_tiyoots_models.objects.all()
    serializer_class    =   tiyootsModelSerializer

class PostLoggedInRetriveApiView(generics.RetrieveAPIView):
    queryset            =   make_tiyoots_models.objects.all()
    serializer_class    =   TiyootLoggedInSerializer
    lookup_field        =   'id'


class PostDestroyApiView(generics.DestroyAPIView):
    queryset            =   make_tiyoots_models.objects.all()
    serializer_class    =   TiyootLoggedInSerializer
    lookup_field        =   'id'


class codeApiListView(generics.ListAPIView):

    def get_queryset(self):
        return make_tiyoots_models.objects.filter(user__username=str(self.kwargs['username']))
    
    serializer_class    =   CodeListApiView
    lookup_field        =   "username"

class TiyootUpdateApiView(generics.UpdateAPIView):
        queryset            =   make_tiyoots_models.objects.all()
        serializer_class    =   UpdateApiView
        lookup_field        =   "id"