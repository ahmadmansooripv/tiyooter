from django import forms


class make_tiyoots_forms(forms.Form):
	headline=forms.CharField(label="Title",max_length=40)
	content=forms.CharField(label='Content',max_length=500,widget=forms.Textarea())


class repositoryMake(forms.Form):
	headline=forms.CharField(label="Page Title",max_length=100,widget=forms.TextInput(attrs={"class":"form-control rounded","placeholder":"Title"}))
	codeFile=forms.FileField(widget=forms.FileInput(attrs={"accept":".py,.cpp,.cs,.java,.js",'class':'custom-file-input',"id":"customFile","placeholder":"codeFile"}))
	description=forms.CharField(label="Description",max_length=300,widget=forms.Textarea(attrs={"class":"form-control rounded","placeholder":"description"}))
	codeTag=forms.CharField(label="Tag",widget=forms.TextInput(attrs={"class":"form-control"}))


	
