from django.contrib import admin
from django.urls import path,include,re_path
from account.views import login,signup
from .views import make_tiyoots,tiyoots,follow,logout,whatsup,repositoryView,codesView


urlpatterns = [
   #re_path(r'^(?P<year>[a-z0-9]+)/',tiyoots),
   #re_path(r'^(?P<username>[a-z0-9]+)/maketiyoots/$',make_tiyoots),
    path('make_tiyoot/',make_tiyoots,name="make"),
    path('tiyoots/',tiyoots,name='tiyoots'),
    path('follow/',follow,name='follow'),
    path('logout/',logout,name="logout"),
    path('whatsup/',whatsup,name="whatsup"),
    path('repository/',repositoryView,name="repo"),
    path('codes/',codesView,name="codes"),
]
