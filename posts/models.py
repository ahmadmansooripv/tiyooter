from django.db import models
from account.models import UserInformation
from datetime import datetime
from django.core.files.storage import FileSystemStorage
from django.conf import settings


class make_tiyoots_models(models.Model):
	user=models.ForeignKey(UserInformation,on_delete=models.CASCADE)
	headline=models.CharField(max_length=40)
	content=models.TextField(max_length=500)
	datetime=models.DateTimeField(default=datetime.now,blank=True)
	def __str__(self):
		string=str(self.headline)
		return string+' '+self.content

class repository(models.Model):
	def updateFilename(instance,filename):
		return settings.BASE_DIR+'/media/'+str(instance.user.username)+"/"+str(filename)
	fs=FileSystemStorage(location="/media/")
	user=models.ForeignKey(UserInformation,on_delete=models.CASCADE)
	headline=models.CharField(max_length=100)
	codeFile=models.FileField(upload_to=updateFilename)
	description=models.CharField(max_length=300)
	datetime=models.DateTimeField(default=datetime.now,blank=False)
	codeTag=models.CharField(max_length=100,blank=False)
	codeExtension=models.CharField(max_length=10,blank=True)

	def __str__(self):
		string=str(self.user.username+" ")+str(self.headline)+" in "+str(self.codeExtension)
		return string
	
