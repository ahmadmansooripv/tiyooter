from django.db import models

from django.db import models

class UserInformation(models.Model):
	username=models.CharField(max_length=30)
	password=models.CharField(max_length=30)
	firstname=models.CharField(max_length=30)
	lastname=models.CharField(max_length=30)
	email=models.EmailField(max_length=70)
	def __str__(self):
		return self.username


class FollowersModel(models.Model):
	user=models.ForeignKey(UserInformation,on_delete=models.CASCADE)
	follower=models.ForeignKey(UserInformation,related_name='follower',on_delete=models.CASCADE)

	def __str__(self):
		return	str(self.follower.firstname)+ " followed "+str(self.user.firstname)






#class Following(models.Model):
