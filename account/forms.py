from django import forms






class UserSignup(forms.Form):
	firstname=forms.CharField(max_length=30,min_length=3,label="First name")
	lastname=forms.CharField(max_length=30,min_length=3,label="Last name",widget=forms.TextInput())
	username=forms.CharField(max_length=30,min_length=3,label="Username",widget=forms.TextInput())
	password=forms.CharField(max_length=30,min_length=8,label="Password",widget=forms.PasswordInput())
	confirm_password=forms.CharField(max_length=30,min_length=8,label="Confirm-Pass",widget=forms.PasswordInput())
	email=forms.EmailField(label="E-mail",required=False,widget=forms.EmailInput())

class UserLogin(forms.Form):
	username=forms.CharField(max_length=30,min_length=3,label='Username')
	password=forms.CharField(max_length=30,min_length=8,label='Password',widget=forms.PasswordInput())
	
