from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect,HttpResponse
from .forms import UserSignup,UserLogin
from .models import UserInformation,FollowersModel
from django.conf import settings
from django.urls import reverse
import re
import os
from datetime import datetime

def checkUN(username):
	patt=re.compile('[^0-9a-z]+')
	a=patt.findall(username)
	if a!=[]:
		return False
	else:
		return  True		

def signup(request):
	flag_is_exist=False
	if request.method=="POST":
		signup_form=UserSignup(request.POST)
		if signup_form.is_valid():
			if checkUN(str(signup_form.cleaned_data['username'])):
				user_info=UserInformation.objects.all()
				for u in user_info:
					if u.username == signup_form.cleaned_data['username']:
						flag_is_exist=True
				if signup_form.cleaned_data['password']==signup_form.cleaned_data['confirm_password'] :
					if flag_is_exist==True:
						message="this username is already taken!!"
						return render(request,"account/signup.html",{'message':message})
					else :
						mod=UserInformation()
						mod.username=signup_form.cleaned_data['username']
						mod.password=signup_form.cleaned_data['password']
						mod.firstname=signup_form.cleaned_data['firstname']
						mod.lastname=signup_form.cleaned_data['lastname']
						mod.email=signup_form.cleaned_data['email']
						mod.save()
						user=UserInformation.objects.get(username=str(signup_form.cleaned_data['username']))
						follow=FollowersModel()
						follow.user=user
						follow.follower=user
						follow.save()
						request.session['is_login']="loggedin"
						request.session['username']=str(signup_form.cleaned_data['username'])
						request.session.set_expiry(30)
						return redirect(reverse('tiyoots',kwargs={'username':str(request.session['username'])}))
				else :
					message="Password does not match the confirm password"
					return render(request,'account/signup.html',{'message':message,'form':signup_form})
			else:
					message = "only lowercase chracter and number"
					return render(request,'account/signup.html',{'message':message,'form':signup_form})
		else :
			message="your informatino is not valid! try again"
			return render(request,'account/signup.html',{'message':message,'form':signup_form})
	else:
		signup_form=UserSignup()
		return render(request,"account/signup.html",{'form':signup_form})



def login(request):
	if request.method == "POST":
		login=UserLogin(request.POST)
		if login.is_valid():
			user_data=UserInformation.objects.all()
			for user in user_data:
				if login.cleaned_data['username'] == user.username and login.cleaned_data['password'] == user.password :
					request.session['is_login']="loggedin"
					request.session['username']=str(login.cleaned_data['username'])
					request.session.set_expiry(30)
					print (datetime.now())
					print (request.session.get_expiry_date())
					return redirect(reverse('tiyoots',kwargs={'username':str(request.session['username'])}))
				else :
					request.session['wrong']=False
			if  (request.session['is_login']=='loggedin') or request.session['wrong'] :
					message="this user is not exist !"
					return render(request,"account/login.html",{'message':message,'form':login})
		else :
			message="your informatino is not valid! try again"
			return render(request,'account/login.html',{'message':message,'form':login})
	else:
		login=UserLogin()
		return render(request,"account/login.html",{'form':login,'message':"welcome"})
