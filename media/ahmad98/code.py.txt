import RPi.GPIO as GPIO
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np
import subprocess as sub


#UNUSED FUNCION
#def distance_to_camera(KNOWNheight, focalLength, objectheight, imageheight, sensorheight):
    # return (KNOWNheight * focalLength * imageheight) / (objectheight * sensorheight)
#for picamera

def bright(frame):
    f=cv2.VideoCapture(0)
    font = cv2.FONT_HERSHEY_SIMPLEX
    gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    t,a=gray.shape
    low_light1=(gray[int(t/2),int(a/2)])
    low_light2=(gray[int(t/4),int(a/4)])
    low_light3=(gray[int(3*t/4),int(3*a/4)])
    low_light4=(gray[int(t/4),int(3*a/4)])
    low_light5=(gray[int(3*t/4),int(a/4)])
    print (low_light1)
    if int(low_light1)<60 and int(low_light2)<60 and int(low_light3)<60 and int(low_light4)<60 and int(low_light5)<60:

        #cv2.putText(frame,'light on !!!',(10,50), font, 2,(255,255,255),2,cv2.LINE_AA)
        return True
    else :
        return False



#p = gp.PWM(12,500)
#p.start(0)
#p.ChangeDutyCycle(70)
#time.sleep(1)
#p.stop()



sensor_height = 2.74

KNOWN_height = 35

image_height = 370

focalLength = 3.6

GPIO.setmode(GPIO.BOARD)
#setup output GPIO
motorRforward = 38
motorRbackward = 32
motorLforward = 40
motorLbackward = 36
Catcheropening = 15
shooter=13
Flashlights = 21
Catcherclosing = 12
motorRFstate = False
motorRBstate = False
motorLFstate = False
motorLBstate = False
Catcheropenedstate = False
Flashlightstate = False
GPIO.setup(motorRforward,GPIO.OUT)
GPIO.setup(motorRbackward,GPIO.OUT)
GPIO.setup(motorLforward,GPIO.OUT)
GPIO.setup(motorLbackward,GPIO.OUT)
GPIO.setup(Flashlights,GPIO.OUT)
GPIO.setup(Catcheropening,GPIO.OUT)
GPIO.setup(Catcherclosing,GPIO.OUT)
GPIO.setup(shooter,GPIO.OUT)

GPIO.output(motorLforward,GPIO.OUT)
GPIO.output(motorRbackward,GPIO.OUT)
# payane setting up
#charkhesh be rast ta yaftane toop
camera=PiCamera()
camera.resolution=(550,370)
camera.framerate=60

rawCapture = PiRGBArray(camera,size=(550,370))


lower=np.array([165,160,100])
upper=np.array([180,255,255])
lower2=np.array([165,160,100])
upper2=np.array([180,255,255])
lowerGoal=np.array([30,50,80])
upperGoal=np.array([45,255,255])


kernel=np.ones((7,7))
p = GPIO.PWM(Catcheropening,500)
p.start(0)
p.ChangeDutyCycle(100)
time.sleep(1)
p.stop()
Catcheropenedstate=True
sub.call(['cvlc','Im Loading My Instructions.mp3','--play-and-exit'])
time.sleep(0.1)
sub.call(['cvlc','Looking for ball.mp3','--play-and-exit'])
#while True
for f in camera.capture_continuous(rawCapture,format='bgr',use_video_port=True):
       # eq -> ret,frame=pic.read()
       #set the center for 1 frame

    frame=f.array
    cX = 275
    cY = 185
    if bright(frame):
        #sub.call(['cvlc','Turning Flash Lights On.mp3','--play-and-exit'])
        Flashlightstate=True
        GPIO.output(Flashlights,True)




    hsv=cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    mask=cv2.inRange(hsv,lower,upper)
    mask_high=cv2.inRange(hsv,lower2,upper2)
    mask=mask+mask_high
    re=cv2.GaussianBlur(mask,(7,7),0)
    re=cv2.morphologyEx(mask,cv2.MORPH_OPEN,kernel)
    re=cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernel)
    re=cv2.GaussianBlur(mask,(7,7),0)
    finall=cv2.bitwise_and(frame,frame,mask=re)
    _,contours,hirachy = cv2.findContours(re,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    area_list=[]
    for c in contours:
        area=cv2.contourArea(c)
        area_list.append(area)
    if len(area_list) :
        maxi=max(area_list)
        #peyda kardan index marboot b bozorgtrin contour
        aa=(area_list.index(maxi))
        cv2.drawContours(frame,contours[aa],-1,(255,255,0),2)
        M=cv2.moments(contours[aa])
        if M['m00'] > 0:
            cX=int(M['m10']/M['m00'])
            #markaz rooye y ha
            cY=int(M['m01']/M['m00'])
            #keshidan yek dayere dar markaze area
            #cv2.circle(mask,(cX,cY),5,(0,255,0),-1)
            cv2.circle(frame,(cX,cY),5,(0,255,0),-1)
        catcher_on=cv2.contourArea(contours[aa])
        if cY >340 and cY<369:
            #catcher inja roshan mishavad
            #70000 bar asase doorbin laptop ast ,bayad bar asas raspberry cam moshakhas shavad
            GPIO.output(motorRbackward,False)
            GPIO.output(motorLbackward,False)
            GPIO.output(motorRforward,False)
            GPIO.output(motorLforward,False)
            sub.call(['cvlc','Closing Catcher.mp3','--play-and-exit'])
            motorRFstate = False
            motorRBstate = False
            motorLFstate = False
            motorLBstate = False
            GPIO.output(Catcheropening,False)
            p = GPIO.PWM(Catcherclosing,500)
            p.start(0)
            p.ChangeDutyCycle(100)
            time.sleep(1)
            p.stop()
            Catcheropenedstate = False
            break
            #will change later



        else:
            #codes for activating motors based on cX
            if cX>200  and  cX<350  and (not motorRFstate or not motorLFstate):
                #sub.call(['cvlc','Ball Found.mp3','--play-and-exit'])
                GPIO.output(motorRbackward,False)
                GPIO.output(motorLbackward,False)
                GPIO.output(motorRforward,True)
                GPIO.output(motorLforward,True)
                motorRFstate = True
                motorRBstate = False
                motorLFstate = True
                motorLBstate = False
            if cX<200 and (not motorRFstate or not motorLBstate) :
                GPIO.output(motorRbackward,False)
                GPIO.output(motorLforward,False)
                GPIO.output(motorRforward,True)
                GPIO.output(motorLbackward,True)
                motorRFstate = True
                motorRBstate = False
                motorLFstate = False
                motorLBstate = True

            if cX>350 and (not motorRBstate  or not motorLFstate) :
                GPIO.output(motorRforward,False)
                GPIO.output(motorLbackward,False)
                GPIO.output(motorRbackward,True)
                GPIO.output(motorLforward,True)
                motorRFstate = False
                motorRBstate = True
                motorLFstate = True
                motorLBstate = False
    else:
        #charkhidan
        GPIO.output(motorRforward,False)
        GPIO.output(motorLbackward,False)
        GPIO.output(motorRbackward,True)
        GPIO.output(motorLforward,True)
        motorRFstate = False
        motorRBstate = True
        motorLFstate = True
        motorLBstate = False
    cv2.imshow('frame',frame)
    #print(distance_to_camera())
    key=cv2.waitKey(1) & 0xFF
    rawCapture.truncate(0)
    if key==ord('q'):
        break



camera.close()
camera=PiCamera()
camera.resolution=(550,370)
camera.framerate=60

rawCapture = PiRGBArray(camera,size=(550,370))






    ######## Goal #########
sub.call(['cvlc','Looking for target.mp3','--play-and-exit'])

for f in camera.capture_continuous(rawCapture,format='bgr',use_video_port=True):
    # eq -> ret,frame=pic.read()
    #set the center for 1 frame
    cX = 275
    cY = 185
    frame=f.array
    if bright(frame):
        #sub.call(['cvlc','Turning Flash Lights On.mp3','--play-and-exit'])
        Flashlightstate=True
        GPIO.output(Flashlights,True)




    hsv=cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    mask=cv2.inRange(hsv,lowerGoal,upperGoal)
    #mask_high=cv2.inRange(hsv,lower2,upper2)
    #mask=mask+mask_high
    re=cv2.GaussianBlur(mask,(5,5),0)
    re=cv2.morphologyEx(mask,cv2.MORPH_OPEN,kernel)
    re=cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernel)
    re=cv2.GaussianBlur(mask,(5,5),0)
    finall=cv2.bitwise_and(frame,frame,mask=re)
    _,contours,hirachy = cv2.findContours(re,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    area_list=[]
    for c in contours:
        area=cv2.contourArea(c)
        area_list.append(area)
    if len(area_list) :
        maxi=max(area_list)
        #peyda kardan index marboot b bozorgtrin contour
        aa=(area_list.index(maxi))
        cv2.drawContours(frame,contours[aa],-1,(255,255,0),2)
        M=cv2.moments(contours[aa])
        if M['m00'] > 0:
            cX=int(M['m10']/M['m00'])
            #markaz rooye y ha
            cY=int(M['m01']/M['m00'])
            #keshidan yek dayere dar markaze area
            #cv2.circle(mask,(cX,cY),5,(0,255,0),-1)
            #cv2.circle(frame,(cX,cY),5,(0,255,0),-1)
            #cv2.rectangle(frame,())

            #catcher_on=cv2.contourArea(contours[aa])
            #if catcher_on>70000:
            #catcher inja roshan mishavad
            #70000 bar asase doorbin laptop ast ,bayad bar asas raspberry cam moshakhas shavad
            #    break
            #else:
            #codes for activating motors based on cX
        if cX>200  and  cX<350  and (not motorRFstate or not motorLFstate):
            #shoot
           # sub.call(['cvlc','target found.mp3','--play-and-exit'])
           # time.sleep(1)
            GPIO.output(motorRbackward,False)
            GPIO.output(motorLbackward,False)
            GPIO.output(motorRforward,False)
            GPIO.output(motorLforward,False)
            sub.call(['cvlc','Opening Catcher.mp3','--play-and-exit'])
            GPIO.output(Catcherclosing,False)
            Catcheropenedstate=True
            #CATCHER IS OPENED READY FOR A SHOOT
            p = GPIO.PWM(Catcheropening,500)
            p.start(0)
            p.ChangeDutyCycle(100)
            time.sleep(1)
            p.stop()
            sub.call(['cvlc','shooting.mp3','--play-and-exit'])
            GPIO.output(shooter,True)
            time.sleep(2)
            GPIO.output(shooter,False)
            break

            pass
        if cX<200 and (not motorRFstate or not motorLBstate) :
            GPIO.output(motorRbackward,False)
            GPIO.output(motorLforward,False)
            GPIO.output(motorRforward,True)
            GPIO.output(motorLbackward,True)
            motorRFstate = True
            motorRBstate = False
            motorLFstate = False
            motorLBstate = True

        if cX>350 and (not motorRBstate  or not motorLFstate) :
            GPIO.output(motorRforward,False)
            GPIO.output(motorLbackward,False)
            GPIO.output(motorRbackward,True)
            GPIO.output(motorLforward,True)
            motorRFstate = False
            motorRBstate = True
            motorLFstate = True
            motorLBstate = False
    else:
        GPIO.output(motorRforward,False)
        GPIO.output(motorLbackward,False)
        GPIO.output(motorRbackward,True)
        GPIO.output(motorLforward,True)
        motorRFstate = False
        motorRBstate = True
        motorLFstate = True
        motorLBstate = False




    cv2.imshow('frame',frame)
    #print(distance_to_camera())
    key=cv2.waitKey(1) & 0xFF
    rawCapture.truncate(0)
    if key==ord('q'):
        break
GPIO.output(motorRbackward,False)
GPIO.output(motorLbackward,False)
GPIO.output(motorRforward,False)
GPIO.output(motorLforward,False)
GPIO.output(Catcheropening,False)
p = GPIO.PWM(Catcherclosing,500)
p.start(0)
p.ChangeDutyCycle(100)
time.sleep(1)
p.stop()
#if(Flashlightstate)
#GPIO.output(Flashlights,False)
GPIO.cleanup()